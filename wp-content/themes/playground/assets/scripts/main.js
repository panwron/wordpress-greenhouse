/* ========================================================================
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {


  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'page_template_template_apply': {
      init: function() {

        $.get("https://api.greenhouse.io/v1/boards/remeapp/jobs/1041218?questions=true", function(data){
                  var application_id = document.createElement('input');
                  $(REME_Expert).append(application_id);
                  $(application_id).attr('type', 'hidden');
                  $(application_id).attr('name', 'id');
                  $(application_id).attr('value', data.id);

                  var questions = [];
                  questions = data.questions;
                  var fields = [];
                  var values = [];

                  for (var i = 0; i < questions.length; i++) {
                      if(questions[i].fields[0].type == 'input_text' && questions[i].label !== 'Referred by' && questions[i].label !== 'Zip Code'){
                          var box = document.createElement('div');
                          $(REME_Expert).append(box);
                          $(box).attr('class', 'box');
                          $(box).attr('id', questions[i].fields[0].name);
                          var label_outer = document.createElement('label');
                          $(box).append(label_outer);
                          $(label_outer).attr('class', 'label_outer');
                      }else{
                          var label_outer = document.createElement('label');
                          $(REME_Expert).append(label_outer);
                          $(label_outer).attr('class', 'label_outer');
                      }

                      if(questions[i].required){
                          $(label_outer).html(questions[i].label + '<span class=\'span\'> *</span>');
                      }else{
                          $(label_outer).html(questions[i].label);
                      }

                      fields = questions[i].fields;
                      for (var j = 0; j < fields.length; j++) {

                          if(fields[j].type == 'input_text' || fields[j].type == 'input_file'){
                              var input = document.createElement('input');
                              $(label_outer).append(input);
                              $(input).attr('name', fields[j].name);
                              if(fields[j].type == 'input_text'){
                                  $(input).attr('type', 'text');
                                  $(input).attr('class', 'long_input');
                              }
                              if(fields[j].type == 'input_file' && fields[j].name == 'resume' || fields[j].type == 'input_file' && fields[j].name == 'cover_letter'){
                                  $(input).attr('type', 'file');
                                  var p = document.createElement('p');
                                  $(REME_Expert).append(p);
                                  $(p).attr('class', 'file_description');
                                  $(p).text('Accepted file types include: csv, doc, docx, pdf, rtf, txt');
                              }
                              if(fields[j].type == 'input_file' && fields[j].name == 'question_8503118'){
                                  $(input).attr('type', 'file');
                                  var p = document.createElement('p');
                                  $(REME_Expert).append(p);
                                  $(p).attr('id', 'driver_license');
                                  $(p).attr('class', 'file_description');
                                  $(p).text('Acceptable forms of Identification Include: Driver\'s License, State Issued ID, Passport');
                              }
                              if(fields[j].type == 'input_file' && fields[j].name == 'question_8503120'){
                                  $(input).attr('type', 'file');
                                  var p = document.createElement('p');
                                  $(REME_Expert).append(p);
                                  $(p).attr('id', 'certification');
                                  $(p).attr('class', 'file_description');
                                  $(p).text('Please make sure that you can clearly see the following: Your Full Name, Certification Number, Expiration Date');
                              }
                              if(fields[j].type == 'input_file' && fields[j].name == 'question_8503128'){
                                  $(input).attr('type', 'file');
                                  var p = document.createElement('p');
                                  $(REME_Expert).append(p);
                                  $(p).attr('id', 'insurance');
                                  $(p).attr('class', 'file_description');
                                  $(p).text('Please note that the following MUST be clearly visible: Insurance Company Name, Your Full Name as Insured, Policy Number, Expiration Date');
                              }
                          }
                          if(fields[j].type == 'textarea' && fields[j].name != 'resume_text'){
                              var textarea = document.createElement('textarea');
                              $(REME_Expert).append(textarea);
                              $(textarea).attr('name', fields[j].name);
                              $(textarea).attr('type', 'textarea');
                          }

                          if(fields[j].type == 'multi_value_single_select'){
                              values = fields[j].values;
                              if(values.length > 0 && values.length < 3){
                                  var div = document.createElement('div');
                                  $(label_outer).append(div);
                                  $(div).attr('class', 'spacer');
                                  for (var k = 0; k < values.length; k++) {
                                      var label_inner = document.createElement('label');
                                      $(div).append(label_inner);
                                      $(label_inner).html(values[k].label);
                                      $(label_inner).attr('class', 'label_inner');
                                      var input = document.createElement('input');
                                      $(label_inner).append(input);
                                      $(input).attr('name', fields[j].name);
                                      $(input).attr('type', 'radio');
                                      $(input).attr('value', values[k].value);
                                  }
                              }
                              if(values.length >= 3){
                                  var select = document.createElement('select');
                                  $(REME_Expert).append(select);
                                  $(select).attr('name', fields[j].name);
                                  $(select).attr('class', 'select');
                                  for(var k = 0; k < values.length; k++){
                                      var option = document.createElement('option');
                                      $(select).append(option);
                                      $(option).html(values[k].label);
                                      $(option).attr('value', values[k].value);
                                  }
                              }
                          }
                      } //fields
                  } //questions
                  var button = document.createElement('input');
                  $(REME_Expert).append(button);
                  $(button).attr('type', 'submit');
                  $(button).attr('value', 'SUBMIT');
                  $(button).attr('id', 'apply');
              });
        $(REME_Expert).validate({
                  rules: {
                      first_name: {
                          required: true,
                          maxlength: 40
                      },
                      last_name: {
                          required: true,
                          maxlength: 40
                      },
                      email: {
                          required: true,
                          email: true
                      },
                      phone:{
                          required: true,
                          phoneUS: true
                      },
                      resume: {
                          required: true,
                          extension: "csv|doc|docx|pdf|rtf|txt"
                      },
                      cover_letter: {
                          extension: "csv|doc|docx|pdf|rtf|txt"
                      },
                      //gender
                      question_8503116: {
                          required: true
                      },
                      //zip_code
                      question_8503117: {
                          required: true,
                          zipcodeUS: true
                      },
                      //applying_for
                      question_8503245: {
                          required: true
                      },
                      //id
                      question_8503118: {
                          required: true
                      },
                      //certified
                      question_8503119: {
                          required: true
                      },
                      //certification
                      question_8503120: {
                          required: true
                      },
                      //insurance
                      question_8503121: {
                          required: true
                      },
                      //policy
                      question_8503128: {
                          required: true
                      },
                      //experience
                      question_8503135: {
                          required: true
                      }
                  },
                  // <!-- end of rules -->
                  messages: {
                      first_name: {
                          required: "Please enter your first name."
                      },
                      last_name: {
                          required: "Please enter your last name."
                      },
                      email: {
                          required: "Email address is required."
                      },
                      phone: {
                          required: "Mobile number is required."
                      },
                      resume: {
                          required: "Please attach your resume."
                      },
                      question_8503116: {
                          required: "Please select a gender."
                      },
                      question_8503117: {
                          required: "Please enter your zip code.",
                          zipcodeUS: "Enter a valid zip code."
                      },
                      //applying_for
                      question_8503245: {
                          required: "Please choose an option."
                      },
                      question_8503118: {
                          required: "Please attach your ID."
                      },
                      question_8503119: {
                          required: "Please choose an option."
                      },
                      question_8503120: {
                          required: "Please attach your certification."
                      },
                      question_8503121: {
                          required: "Please choose an option."
                      },
                      question_8503128: {
                          required: "Please attach your policy."
                      },
                      question_8503135: {
                          required: "Please select a level of experience."
                      }

                  },
                  // <!--end of error message-->
                  errorPlacement: function(error, element){
                      if(element.attr('type') == 'radio'){
                          error.insertAfter(element.closest('label.label_outer'));
                          error.css('font-weight', '900px');
                      }else{
                          error.insertAfter(element);
                      }
                  },
                  wrapper: 'li',
                  submitHandler: function(form) {
                      form.submit();
                  }
              });


      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
